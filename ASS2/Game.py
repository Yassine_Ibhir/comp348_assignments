# Yassine Ibhir 40251116

FREQUENCIES = {
    "a": 8.17, "b": 1.49, "c": 2.78, "d": 4.24, "e": 12.70,
    "f": 2.23, "g": 2.02, "h": 6.09, "i": 6.97, "j": 0.15,
    "k": 0.77, "l": 4.03, "m": 2.41, "n": 6.75, "o": 7.51,
    "p": 1.93, "q": 0.10, "r": 5.99, "s": 6.33, "t": 9.06,
    "u": 2.76, "v": 0.98, "w": 2.36, "x": 0.15, "y": 1.97,
    "z": 0.07,
}


class Game:
    game = 0
    word = ''
    status = ''
    badGuesses = 0
    missedLetters = 0
    word_before_guess = ''
    score = 0

    def __init__(self, game, word, status,
                 bad_guesses, missed_letters,
                 word_before_guess):
        self.game = game
        self.word = word
        self.status = status
        self.badGuesses = bad_guesses
        self.missedLetters = missed_letters
        self.word_before_guess = word_before_guess
        self.process_score()

    def process_score(self):

        if self.status == 'Success':
            self.calculate_success_score()
        else:
            self.calculate_lost_score()

    def calculate_success_score(self):
        success_score = self.blank_letter_score_sum()
        success_score = success_score / self.missedLetters
        final_success_score = success_score - ((10 * self.badGuesses) / 100) * success_score
        self.set_score(final_success_score)

    def calculate_lost_score(self):
        final_lost_score = self.blank_letter_score_sum() * (-1)
        self.set_score(final_lost_score)

    def blank_letter_score_sum(self):
        blank_letter_sum = 0
        print(self.word)
        print(self.word_before_guess)
        for i in range(0, 4):
            if self.word[i] != self.word_before_guess[i]:
                blank_letter_sum += FREQUENCIES.get(self.word[i])
        return blank_letter_sum

    def set_score(self, final_score):
        self.score = round(final_score, 2)
