# Yassine Ibhir 40251116

import sys

from Guess import Guess
from StringDatabase import StringDatabase


def main():
    # Check if the correct number of command line arguments is provided
    if len(sys.argv) == 2:
        game_mode = sys.argv[1]
        if game_mode != 'test' and game_mode != 'play':
            print("Run The game with a valid arguments")
            sys.exit(1)

        db = StringDatabase()
        db.load_words()
        guess = Guess(db, game_mode)
        guess.start_guess()


if __name__ == "__main__":
    main()
