# Yassine Ibhir

import random


class StringDatabase:
    four_string_words = []

    def load_words(self):
        # opening the file in read mode
        four_letters = open("four_letters.txt", "r")

        # reading the file
        file_data = four_letters.read()

        # replacing end of line('/n') with ' ' and
        words_without_line = file_data.replace('\n', ' ')
        # splitting the text it further when '.' is seen.
        self.four_string_words = words_without_line.split(' ')

        # Removing the last element with space
        self.four_string_words = self.four_string_words[:len(self.four_string_words) - 1]
        four_letters.close()

    def get_random_word(self):
        index = random.randint(0, len(self.four_string_words))
        return self.four_string_words[index - 1]
