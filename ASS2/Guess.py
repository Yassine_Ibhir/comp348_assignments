# Yassine Ibhir 40251116

import os

from Game import Game


class Guess:
    game_mode = 'play'

    current_word = ""

    current_guess: str = '----'

    letters_guessed = []

    word_guessed = ""

    guesses = []

    is_game_finished = False

    db = None

    game_number = 1

    incorrect_guess = 0

    games = []

    def __init__(self, db, game_mode):
        self.db = db
        self.game_mode = game_mode

    def start_guess(self):

        print("$$")
        print("$$ Guess it or miss it")
        print("$$ \n")

        self.current_word = self.db.get_random_word()
        while not self.is_game_finished:

            if self.word_guessed == self.current_word:
                # update State (Get the points and store it)
                self.current_word = self.db.get_random_word()
                self.current_guess = '----'
                self.letters_guessed = []
                self.guesses = []
                self.word_guessed = ''
                self.game_number += 1
                self.incorrect_guess = 0
            self.display_menu()

        self.print_Report()

    def display_menu(self):
        print("Game Number : ", self.game_number)
        while True:
            if self.game_mode == 'test':
                print("Current Word : ", self.current_word)

            print("Current Guess : ", self.current_guess)
            print("Letters Guessed : ", self.letters_guessed)

            print("g = guess, t = tell me, l for a letter, and q to quit \n")
            option = input("Enter your Option: ").lower()

            while option not in "gtlq":
                option = input("Invalid Option. Please re-enter: ").lower()

            if option == 'l':
                valid = self.choose_letter()
                if valid:
                    self.check_letter()
            elif option == 'g':
                self.choose_word()

            elif option == 't':
                self.reveal_word()

            if option == 'q':
                self.is_game_finished = True

            input("Press any key to continue...")
            os.system('clear')

            if option != 'l':
                break

    def choose_letter(self):
        while True:
            letter = input("Choose a letter: ").lower()
            if letter.isalpha():
                if letter in self.letters_guessed:
                    print("you have already guessed the letter...")
                    return False
                self.letters_guessed.append(letter)
                return True
            print("That is not a Letter...")

    def choose_word(self):
        while True:
            word = input("what's your guess (4 letters) : ").lower()
            if word.isalpha():
                self.word_guessed = word
                if self.word_guessed == self.current_word:
                    game = Game(self.game_number, self.current_word, 'Success',
                                self.incorrect_guess,
                                self.num_of_missed_letters(), self.current_guess, )
                    self.games.append(game)
                    self.print_feedback("You are Right !")
                elif self.word_guessed in self.guesses:
                    self.print_feedback("You have already guessed this word!")

                else:
                    self.guesses.append(self.word_guessed)
                    self.print_feedback("Wrong Guess, Sorry!")
                    self.incorrect_guess += 1
                break
            print("invalid word try again...")

    def check_letter(self):
        if self.letters_guessed[len(self.letters_guessed) - 1] in self.current_word:
            self.update_current_guess(self.letters_guessed[len(self.letters_guessed) - 1])
            self.print_feedback("The letter is a match !")
        else:
            self.print_feedback("The letter is not a match, Sorry!")

    def reveal_word(self):
        game = Game(self.game_number, self.current_word, 'Gave up',
                    self.incorrect_guess,
                    self.num_of_missed_letters(), self.current_guess)
        self.games.append(game)
        self.print_feedback("You should try harder, the word is... " + self.current_word)
        self.word_guessed = self.current_word

    def print_feedback(self, message):
        print("@@")
        print("@@ FEEDBACK : ", message)
        print("@@")

    def update_current_guess(self, letter):
        for i in range(len(self.current_word)):
            if self.current_word[i] == letter:
                modified_string = self.current_guess[:i] + letter + self.current_guess[i + 1:]
                self.current_guess = modified_string

    def num_of_missed_letters(self):
        count = 0
        for i in range(len(self.letters_guessed)):
            if self.letters_guessed[i] not in self.current_guess:
                count += 1
        return count if count > 0 else 1

    def print_Report(self):

        print("++\n++ Game Report\n++\n")
        print("Game     Word     Status     Bad Guesses     Missed letters     Score")
        print("----     ----     ------     -----------     --------------     -----")

        final_score = 0

        for i in range(len(self.games)):
            final_score += self.games[i].score

            print(self.games[i].game, "      ", self.games[i].word, "   ", self.games[i].status, "  ",
                  self.games[i].badGuesses,
                  "             ", self.games[i].missedLetters, "                ", self.games[i].score)

        print("\nFinal Score : ", round(final_score,2))
