// Yassine Ibhir
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "defines.h"
#include <math.h>
#include "io.h"
#include "sort.h"


// Declarations and Definitions

char** readFile(char fileName[],int size);

void writeFile(char** table);

void sortArr(int col);

int  indexCodes[] = {398, 270, 925, 867, 209, 429, 908, 997, 444, 219};

char COLUMN_HEADER[8][30] =
{ "User ID \0",
  "First Name \0",
  "Last Name \0",
  "Country \0",
  "Phone \0",
  "Email \0",
  "SIN \0",
  "Password \0"			
};


char** suffixes;
char** countries;
char** firstNames;
char** lastNames;
char** sinNum;
char** phoneNumber;
char** email;
char** password;

char** table_first_names;

char** table_last_names;

char** table_countries;

char** userId;
char** table;

extern struct tableInfo info;

char* generateDigits(int num);
char* generatePhone();
char* generateEmail(int idx);
char* generatePassword();
char* generateFirstName();
char* generateLastName();
char* generateCountry();

char* generateUserId(int n);

int randomIndex(int bound);

void generateFirstLastEmail();

void generateTable();

void freeMemory();

void generateFirstNames();

void generateLastNames();

void generateEmails();


// This method generates all first names 
// and stores value in array based on number of rows
void generateFirstNames(){
	for(int i = 0; i< info.numOfRows;i++){

		table_first_names[i] = generateFirstName();

	}
}

// Generates last names and stores value in an array
void generateLastNames(){
	for(int i=0; i<info.numOfRows;i++){
		table_last_names[i] = generateLastName();

	}
}

// Generates emails and puts values in array
void generateEmails(){
	for(int i=0; i<info.numOfRows;i++){
		email[i] = generateEmail(i);

	}
}


// This method is defining the column to store amongs
// first name, last name , and email.
// the order matters since the columns should be consistent
void generateFirstLastEmail(){
	if(info.toSort == 2){
		generateFirstNames();
		// sort first name
		sortArr(2);
		generateLastNames();
		generateEmails();
	}
	else if(info.toSort == 3){
		generateLastNames();
		//sort last name
		sortArr(3);
		generateFirstNames();
		generateEmails();
	}
	else if(info.toSort == 6){
		generateFirstNames();
		generateLastNames();
		//sort both first and last
		sortArr(2);
		sortArr(3);
		generateEmails();

	}
	else{
		generateFirstNames();
		generateLastNames();
		generateEmails();

	}
}

// Frees allocating memory
void freeMemory(){

	for (int i=0;i<1000;i++){
        	free(firstNames[i]);
		firstNames[i] = NULL;
		free(lastNames[i]);
		lastNames[i] = NULL;
	}
	for (int i=0;i<195;i++){
        	free(countries[i]);
		countries[i] = NULL;
	}

	for (int i=0;i<100;i++){
        	free(suffixes[i]);
		suffixes[i] = NULL;
	}
	for(int i =0;i<info.numOfRows;i++){
		free(email[i]);
		email[i] =NULL;
	}
	for(int i =0;i<info.numOfRows;i++){
		free(password[i]);
		password[i] = NULL;
	}
	for(int i =0;i<info.numOfRows;i++){
		free(phoneNumber[i]);
		phoneNumber[i] = NULL;
	}

	for(int i =0;i<info.numOfRows;i++){

		free(sinNum[i]);
		sinNum[i] = NULL;
	}


	for(int i =0;i<info.numOfRows;i++){
		free(userId[i]);
		userId[i]=NULL;
	}
	for(int i =0;i<info.numOfRows;i++){

		free(table[i]);
		table[i] = NULL;
	}

}

// This the griver method of the thsi file
// it is invoked from outside to orchestrate
// the generation of table

void generateFile(){


	printf("\n");
	srand(time(NULL));

	// initialize all the data needed for the tables
	//(reading and initializing arrays
	suffixes = readFile("./comp348.2023f.a01.assignment.i-data/email_suffixes.txt",100);
	countries = readFile("./comp348.2023f.a01.assignment.i-data/countries.txt",195);
	firstNames = readFile("./comp348.2023f.a01.assignment.i-data/first_names.txt",1000);
	lastNames = readFile("./comp348.2023f.a01.assignment.i-data/last_names.txt",1000);

	// Allocate memory for all columns
	sinNum = (char**) malloc((info.numOfRows) * sizeof(char*));

	phoneNumber = (char**) malloc((info.numOfRows) * sizeof(char*));

	email = (char**) malloc((info.numOfRows) * sizeof(char*));

	password = (char**) malloc((info.numOfRows) * sizeof(char*));

	table_first_names = (char**) malloc((info.numOfRows) * sizeof(char*));

	table_last_names = (char**) malloc((info.numOfRows) * sizeof(char*));

	table_countries = (char**) malloc((info.numOfRows) * sizeof(char*));

	userId = (char**) malloc((info.numOfRows) * sizeof(char*));

	// generate first name last name and email and sort if necessary
	generateFirstLastEmail();
	//Generate the other columns
	for(int i=0;i<info.numOfRows;i++){

		userId[i] = generateUserId(i+1);

		table_countries[i] = generateCountry();

		phoneNumber[i] = generatePhone();

		sinNum[i] = generateDigits(9);

		password[i] = generatePassword();
	}
	// We call sort to see if the user wants to sort columns with range of 4 and 8(see sort method)
	sortArr(4);
	// Alocate memory to table
	table = (char**) malloc((info.numOfRows+2) * sizeof(char*));
	// Generate Table
	generateTable();
	// Write table to the file
	writeFile(table);
	// Free memory
	freeMemory();

	printf(" \n");
	printf("Summary of properties:  \n");
	printf("  Columns: ");
	for(int i=0; i<info.numOfColumns;i++){
		printf("(%d)%s, ",info.columns[i],COLUMN_HEADER[info.columns[i]-1]);
	}
	printf(" \n");

        printf("  Number of Rows: %d  \n",info.numOfRows);
	printf("  File name: %s  \n",info.outputFile);
	printf("  Column To sort: (%d) %s \n ",info.toSort,COLUMN_HEADER[info.toSort -1]);
	printf(" \n");
	printf("Table Written successfully to %s \n",info.outputFile);
        char proceed;

	printf("\nPress 'c' or 'C' to continue ");

	do {
		scanf("%c", &proceed);
        } while ( (proceed != 'c') && (proceed != 'C'));

}

// randomly Generate n digits phone/sin
char* generateDigits(int num){
	char* digits = malloc(num+1 * sizeof(char));

	for(int i =0;i<num;i++){
		int g = rand()%10;
		char v = g + '0';
		digits[i] = v;
	}
	digits[num] = 0;

	return digits;
}

// return random index within 0 and bound
int randomIndex(int bound){
	int index = rand()%bound;
	return index;
}

// Generate Phone number
char* generatePhone(){

	char* phone = malloc(9 * sizeof(char));

	int i = randomIndex(10);

	int phoneIndex = indexCodes[i];
	sprintf(phone,"%d", phoneIndex);
	strcat(phone,"-");
	char* fourDigits = generateDigits(4);
	strcat(phone,fourDigits);
	phone[8] = 0;
	free(fourDigits);
	return phone;
}

// Generate Email
char* generateEmail(int idx){

	char* email = malloc(60 * sizeof(char));

        char firstN = table_first_names[idx][0];
	email[0] = firstN;
	email[1] = 0;
	char* lastName = table_last_names[idx];

	int j = randomIndex(100);
	char* sufi = suffixes[j];

	strcat(email,lastName);
	strcat(email,"@");
	strcat(email,sufi);

	return email;
}

// Generate a passord between 6 and 16 chars
char* generatePassword(){
	int numOfChars = randomIndex(17);

	if(numOfChars < 6) numOfChars = 6;

    	char numbers[] = "0123456789"; 

    	char letter[] = "abcdefghijklmnoqprstuvwyzx"; 

    	char LETTER[] = "ABCDEFGHIJKLMNOQPRSTUYWVZX"; 

    	char symbols[] = "!@#$%^&*?()-=+<>.;:']{]{|`~"; 

	int randomy = randomIndex(4);

	char* paswd = malloc(numOfChars+1 * sizeof(char));
	paswd[numOfChars] = 0;

	for(int i =0; i<numOfChars;i++){
		if(randomy == 1){
			paswd[i] = numbers[randomIndex(10)];
			randomy = randomIndex(4);	
		}
		else if(randomy == 2){
			paswd[i] = letter[randomIndex(26)];
			randomy = randomIndex(4);	
		}
		else if(randomy == 3){
			paswd[i] = symbols[randomIndex(27)];
			randomy = randomIndex(4);	
		}
		else{
			paswd[i] = LETTER[randomIndex(26)];
			randomy = randomIndex(4);	
		}
	}
	return paswd;

}

// randomly pick one first name from 1000 values
char* generateFirstName(){
	int ran = randomIndex(1000);
	char* fName = firstNames[ran];
	return fName;
}

//randomly pick one last name from 1000
char* generateLastName(){

	int ran = randomIndex(1000);
	char* lName = lastNames[ran];
	return lName;

}

// pick one country from 195 countries
char* generateCountry(){

	int ran = randomIndex(195);
	char* country = countries[ran];
	return country;

}
//generate userid as a string using log method
// to define number of digits
char* generateUserId(int n){
	// log10 method belongs to math.h but must be
	// compiled with -lm flag since gcc does not link it
	//automaticaly(to get number of digits in the userId=size of array)
	int numOfDigits = log10(n)+1;
	char* uId = malloc(numOfDigits+1 * sizeof(char));
	uId[numOfDigits] = 0;
	sprintf(uId,"%d", n);
	return uId;

}

// generate table with all columns user asked for
void generateTable(){

	char * header=malloc(200 *sizeof(char));
	header[0] = 0;
	// Add header first
	for(int l=0;l<info.numOfColumns;l++){

		strcat(header, COLUMN_HEADER[info.columns[l]-1]);
		if(l <info.numOfColumns-1){
		strcat(header, ",");
		}
	}
	strcat(header,"\n\0");
	table[0]=  header;
	// Add all rows
	int i; // access values of rows starting from i=0(names,emails...)
		// but storing rows starting from i=1 since header is in i=0.
	for(i=0;i<info.numOfRows;i++){
		char * row=malloc(200 *sizeof(char));

		row[0] = 0;
		int j;
		for(j=0;j<info.numOfColumns;j++){

			int colm = info.columns[j];
			if(colm == 1){
				strcat(row,userId[i]);
				if(j <info.numOfColumns-1){
				strcat(row, ",");
				}
			}
			if(colm == 2){
				strcat(row,table_first_names[i]);
				if(j <info.numOfColumns-1){
				strcat(row, ",");
				}
			}
			if(colm == 3){
				strcat(row,table_last_names[i]);
				if(j <info.numOfColumns-1){
				strcat(row, ",");
				}
			}
			if(colm == 4){
				strcat(row,table_countries[i]);
				if(j <info.numOfColumns-1){
				strcat(row, ",");
				}

			}
			if(colm == 5){
				strcat(row,phoneNumber[i]);
				if(j <info.numOfColumns-1){
				strcat(row, ",");
				}
			}

			if(colm == 6){
				strcat(row,email[i]);
				if(j <info.numOfColumns-1){
				strcat(row, ",");
				}

			}
			if(colm == 7){
				strcat(row,sinNum[i]);
				if(j <info.numOfColumns-1){
				strcat(row, ",");
				}

			}
			if(colm == 8){
				strcat(row,password[i]);
				if(j <info.numOfColumns-1){
				strcat(row, ",");
				}

			}
		}

		strcat(row, "\n\0");
		int length = strlen(row);
		//i+1 because header is in position 0
		table[i+1] = malloc((length+1) *sizeof(char));
		table[i+1]= row;

	}

}

