//Yassine Ibhir 40251116

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defines.h"

// External global values
extern char** sinNum;
extern char** phoneNumber;
extern char** password;
extern char** table_first_names;
extern char** table_last_names;
extern char** table_countries;

int compareStr(const void* elem1, const void* elem2);

extern struct tableInfo info;

// Method sorts array
void sortArr(int col){
	//if it's not first name or last name
	if(col>3){
		// we set the column to sort value
		col = info.toSort;
	}
	switch(col){
		case 2:
			qsort(table_first_names, info.numOfRows, sizeof(char*), compareStr);
			break;
		case 3:
			qsort(table_last_names, info.numOfRows, sizeof(char*), compareStr);
			break;
		case 4:
			qsort(table_countries, info.numOfRows, sizeof(char*), compareStr);
			break;
		case 5:
			qsort(phoneNumber, info.numOfRows, sizeof(char*), compareStr);
			break;
		case 7:
			qsort(sinNum, info.numOfRows, sizeof(char*), compareStr);
			break;
		case 8:
			qsort(password, info.numOfRows, sizeof(char*), compareStr);
			break;
	}

}
// Compare methods used in the qsort method
int compareStr(const void* elem1, const void* elem2)  
{
    return strcmp(*(char**)elem1, *(char**)elem2);  

}
