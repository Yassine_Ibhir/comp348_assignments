//Yassine Ibhir

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defines.h"
// access table info
extern struct tableInfo info;

char* cleanLine(char line[],int size);
void writeFile(char** table);

// Method reads file and stores value in an array of strings
char** readFile(char fileName[], int numberOfLines){

	FILE *inputFile;
	inputFile =  fopen(fileName,"r");
	char** lines = (char**) malloc(numberOfLines * sizeof(char*));
	char line[60];
	int index =0;
	while (fgets(line, sizeof(line), inputFile)) {
		lines[index] = cleanLine(line,60);
		index++;
	}
	fclose( inputFile );
	inputFile = NULL;  // Safety precaution, to prevent trying to use a closed file.
	return lines;
}


// Method return a line without new line character(10 is ascii for \n)
char* cleanLine(char line[],int size){
		char* buffer;

		for(int i=0;i<size;i++){

		if(line[i] == 10){
			buffer = (char*) malloc(i * sizeof(char));
    			//copy line without new line character
			buffer = strndup( line, i );
			break;
		}
	}
	return buffer;
}

// Method writes strings to a file
void writeFile(char** table){
    	FILE *fp;

	fp = fopen(info.outputFile,"w");

	for(int i=0;i<info.numOfRows+1;i++){
		fputs(table[i], fp);
	}

    	fclose(fp);
	fp = NULL;

}
