//Yassine Ibhir 40251116

//Header file for io.c
// accessible methods
#ifndef IO_H_   /* Include guard */
#define IO_H_

char** readFile(char fileName[], int numberOfLines);
void writeFile(char** table); 

#endif  
