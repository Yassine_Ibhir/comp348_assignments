// Yassine Ibhir 40251116

// Struct holds all information about the table
#ifndef DEFINE_H_
#define DEFINE_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

struct tableInfo {
    int  columns[10];
    int numOfColumns;	
    char outputFile[30];
    int32_t numOfRows;
    int toSort;
};


#endif
