#include <stdio.h>
#include <stdlib.h>
// Yassine Ibhir 40251116

#include "generate.h"
#include "defines.h"
#include "io.h"
#include "string.h"
void mainMenu();

//void generateFile();

//void columnMenu();

//void parseColumns(char col[], int size);

struct tableInfo info;

// Main method
int main(){

	mainMenu();

	return 0;
}

// Display menu to allow user  to enter info about table
// to generate
void columnMenu(){
	system("clear");
	printf("\n");
	printf("Column Options \n");
	printf("------------- \n");
	printf(" 1. User Id          5. Phone Number \n");
	printf(" 2. First Name       6. Email address \n");
	printf(" 3. Last Name        7. SIN \n");
	printf(" 4. Country          8. Password \n");
	printf("------------- \n");
	printf("Enter column list(comma-separated,no spaces) : \n");
	char columnInput[20];
	memset(columnInput, 0, sizeof(columnInput));
	scanf("%s",columnInput);
	columnInput[19] = 0;
	parseColumns(columnInput,20);
	info.toSort = info.columns[0];
	printf("Enter Row Count (1<n<1M) : \n");
	scanf("%d",&info.numOfRows);
	printf("Enter output file name (no suffix) : \n");
	scanf("%s",info.outputFile);
	strcat(info.outputFile,".csv");
	generateFile();

}

// displays the main menu and allow user to choose between
// generating a table or exiting the program.
void mainMenu(){
	int exit = 2;
	int selection;
	do{
		system("clear");
		printf("\n");
		printf("TableGen Menu \n");
		printf("------------- \n");
		printf(" 1. Generate new table \n");
		printf(" 2. Exit \n");
		printf("Selection : ");
		scanf("%d",&selection);
		printf("------------- \n");
		switch(selection){
			case 1: 
			columnMenu();
			break;
			case 2:
			printf("\n Goodbye and thanks for using TableGen \n");
			break;
			default:
			printf(" \n Wrong Selection try again \n");
			break;
		}

	}
	while (selection != exit);
}


// convert strings into numbers and removes the semi-comma.
void parseColumns(char col[], int size){
	info.numOfColumns = 0;
	memset(info.columns, 0, sizeof(info.columns));
	for(int i=0;i<size;i++){
		if(col[i] != 44){
	          char choice[2]= {0,0};
 		  choice[0] = col[i];
	          choice[1] = 0;
		  int tmp = atoi(choice);
		  if(tmp>0 && tmp<9){
			info.columns[info.numOfColumns] = tmp;
			info.numOfColumns++;}
		}
	}
}


