;; Yassine Ibhir #40251116

(ns a3
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io]))
  ; this is where you would also include/require the compress module

(def frequency (str/split (slurp "./frequency.txt") #" " -1))

; This method returns vector containing index of frequency
;; That matches value of the word in the file
(defn create_matching_index
  [file_vector]
  (vec (for [item file_vector]
         (let [index_frequence (.indexOf frequency item)]
           (if (= index_frequence  -1) item index_frequence)))))


;; Convert string into integer
(defn try-parse-int [s]
  (try
    (Integer/parseInt s)
    (catch NumberFormatException _ s)))

;; Surrounds number with @ symbol
(defn addAtSignToNumbers [s]
  (if (re-matches #"\d+" s)
    (str "@" s "@")
    s))

;; removes the @ from the number
(defn removeAtSignFromNumbers [s]
  (if (re-matches #"^\@\d+\@$" s)
   (subs s 1 (dec (count s)))
   s))

(defn create_matching_word
  [file_vector]
   (vec (for [item file_vector] 
          (let [index (try-parse-int item)] 
            (if (integer? index) 
              (get frequency index) 
              item)))))

; This methods splits the words and store them in vector
(defn splitWordsFile
  [file_name]
  (let [contents (slurp file_name)]
    (str/split contents #"\s+|(?<=[^\w\s@])@(\d+)@|(?<=[^\w\s])|(?<=[.,()\[\]])|(?<=[\w])(?=[.,()\[\]])|(?<=[.,()\[\]])(?=[\w])|(?<=[\w])(?=[.,()\[\]\?]|$)") 
    ))

; This methods splits the words and store them in vector
(defn splitDecompressedWordsFile
  [file_name]
  (let [contents (slurp file_name)]
    (str/split contents #"\s") 
    ))

; Compress file 
(defn compressFile
  [file_name]
  (let [file_vector (map addAtSignToNumbers (splitWordsFile file_name))] 
    (spit (str file_name ".ct") (str/join " " (map str (create_matching_index file_vector))))))

; Compress file 
(defn decompressFile
  [file_name]
  (let [file_vector  (create_matching_word (splitDecompressedWordsFile file_name))]
    (println  (reduce
             (fn [acc elem]
               (str acc (if (re-matches #"[.,\[\]()?]" elem)
                          elem
                          (str " " elem))))
             "" (map removeAtSignFromNumbers file_vector)) )))


; Display the menu and ask the user for the option
(defn showMenu
  []
  (println "\n\n*** Compression Menu ***")
  (println "------------------\n")
  (println "1. Display list of files")
  (println "2. Display file contents")
  (println "3. Compress a file")
  (println "4. Uncompress a file")
  (println "5. Exit")
  (do
    (print "\nEnter an option? ")
    (flush)
    (read-line)))


; Display all files in the current folder
(defn option1
  []
  (let [f (io/file ".")
        fs (file-seq f)]
    (println "File List:")
    (doseq [item fs]
      (println (.getName item)))))

; Read and display the file contents (if the file exists). Java's File class can be used to 
; check for existence first. 
(defn option2
  []
  (print "\nPlease enter a file name => ")
  (flush)
  (let [file_name (read-line)]
    (let [is_exist (.exists (io/file file_name))]
      (if is_exist (print (slurp file_name)) (print "File does not exist")))))

; Compress the (valid) file provided by the user. You will replace the println expression with code 
; that calls your compression function
(defn option3
  [] ;parm(s) can be provided here, if needed
  (print "\nPlease enter a file name => ")
  (flush)
  (let [file_name (read-line)]
    (let [is_exist (.exists (io/file file_name))]
      (if is_exist
        (compressFile file_name)
        (print "File does not exist")))))


; Decompress the (valid) file provided by the user. You will replace the println expression with code 
; that calls your decompression function
(defn option4
  [] ;parm(s) can be provided here, if needed
  (print "\nPlease enter a file name => ")
  (flush)
  (let [file_name (read-line)]
    (let [is_exist (.exists (io/file file_name))]
      (if is_exist
        (decompressFile file_name)
        (print "File does not exist")))))

; If the menu selection is valid, call the relevant function to 
; process the selection
(defn processOption
  [option] ; other parm(s) can be provided here, if needed
  (if (= option "1")
    (option1)
    (if (= option "2")
      (option2)
      (if (= option "3")
        (option3)  ; other args(s) can be passed here, if needed
        (if (= option "4")
          (option4)   ; other args(s) can be passed here, if needed
          (println "Invalid Option, please try again"))))))


; Display the menu and get a menu item selection. Process the
; selection and then loop again to get the next menu selection
(defn menu
  [] ; parm(s) can be provided here, if needed
  (let [option (str/trim (showMenu))]
    (if (= option "5")
      (println "\nGood Bye\n")
      (do
        (processOption option)
        (recur))))); other args(s) can be passed here, if needed


; ------------------------------
; Run the program. You might want to prepare the data required for the mapping operations
; before you display the menu. You don't have to do this but it might make some things easier

(menu) ; other args(s) can be passed here, if needed
