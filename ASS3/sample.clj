(ns sample
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io]))
 

(defn outer
	[arg1]
	(fn [] (* arg1 2 ))
)

(def c1 (outer 4))

(print (c1))

; Clojures
(defn messenger-builder 
  [greeting]
 (fn [who] (println greeting who)))

(def hello-er (messenger-builder "Hello"))
(def greet-er (messenger-builder "Greetings"))
(hello-er "Shubham")
(greet-er "Shubham")
